module.exports = {
  siteMetadata: {
    title: `Human Stack`,
    description: `Tech collective with loving intentions.`,
    author: `@humanstack`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `human-stack-website`,
        short_name: `hsweb`,
        start_url: `/`,
        background_color: `#ffe5db`,
        theme_color: `#ffe5db`,
        display: `minimal-ui`,
        icon: `src/images/human-stack-icon.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // 'gatsby-plugin-offline',
  ],
}
