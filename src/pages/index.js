import React from "react"
import { Link } from "gatsby"
import styled from "styled-components"
import { useSpring, animated } from "react-spring"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const Heading = styled.h1`
  font-size: 3rem;
  opacity: 0.6;
  text-align: center;
  font-family: Syncopate;
  letter-spacing: 10px;
  color: #830a48;
  max-width: 90vw;
  line-height: 1.3;
  transition: 0.5s ease;
  cursor: none;
  text-shadow: 0px 9px 0px #8fa2ff;

  &:hover {
    opacity: 1;
    line-height: 1.6;
  }

  @media (min-width: 768px) {
    font-size: 5rem;
  }
  @media (min-width: 1025px) {
    font-size: 6rem;
  }
`

const IndexPage = () => {
  const props = useSpring({
    transform: "translateY(0) scale(1) rotate(0deg)",
    from: {
      transform: "translateY(100vh) scale(2.5) rotate(70deg)",
      transformOrigin: "top left",
    },
    config: { mass: 8, tension: 180, friction: 25, easing: 'ease-in-out', precision: 0.001 },
  })
  return (
    <Layout>
      <SEO title="Home" keywords={[`collective`, `tech`, `human`]} />
      <animated.div style={props}>
        <Heading onMouseEnter>
          Human
          <br /> Stack
        </Heading>
      </animated.div>
    </Layout>
  )
}

export default IndexPage
